package org.dmdev.penseller.database;


import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import org.dmdev.penseller.database.entity.*;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;

public class TestDataImporter {

    @Transactional
    public void importData(EntityManagerFactory entityManagerFactory) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Company waterman = saveCompany(entityManager, "Waterman");
        Company pilot = saveCompany(entityManager, "Pilot");
        Company waldmann = saveCompany(entityManager, "Waldmann");
        Company kaweco = saveCompany(entityManager, "Kaweco");

        User billGates = saveUser(entityManager, "Bill", "Bill", "Gates",
                LocalDate.of(1955, Month.OCTOBER, 28), Role.ADMIN);
        User steveJobs = saveUser(entityManager, "Steve", "Steve", "Jobs",
                LocalDate.of(1955, Month.FEBRUARY, 24), Role.USER);
        User timCook = saveUser(entityManager, "Tim", "Tim", "Cook",
                LocalDate.of(1960, Month.NOVEMBER, 1), Role.SELLER);
        User sergeyBrin = saveUser(entityManager, "Sergey", "Sergey", "Brin",
                LocalDate.of(1973, Month.AUGUST, 21), Role.USER);
        User dianeGreene = saveUser(entityManager, "Diane", "Diane", "Greene",
                LocalDate.of(1955, Month.JANUARY, 1), Role.SELLER);

        Pen namiki = savePen(entityManager, "Namiki", 1448, 10,
                "The Emperor of Namiki is a true work of art made by" +
                        " kokkokai artisans in Japan with Roiro Urushi Technique.",
                pilot);
        Pen man = savePen(entityManager, "MAN", 1608, 116,
                "To mark the auspicious milestone of Waterman’s 140th" +
                        " anniversary, the iconic MAN 100 pen is being relaunched as an" +
                        " even more powerful version of itself, the MAN 140 !",
                waterman);
        Pen custom = savePen(entityManager, "Custom", 210, 43,
                "Plunger Inhalation Mechanism Fountain Pen that meets the" +
                        " needs of fountain pen lovers.",
                pilot);
        Pen jubilee = savePen(entityManager, "Jubilee", 887, 11,
                "Limited Edition of 105 pieces worldwide for the 105th anniversary" +
                        " of the company’s founding.",
                waldmann);
        Pen sterling = savePen(entityManager, "Sterling", 1185, 55,
                "Exclusive Kaweco Sport fountain pen made from 56 gram pure 925 Sterling Silver.",
                kaweco);

        Offer namikiOffer = saveOffer(entityManager, pilot, timCook, namiki);
        Offer manOffer = saveOffer(entityManager, waterman, dianeGreene, man);
        Offer customOffer = saveOffer(entityManager, pilot, timCook, custom);
        Offer jubileeOffer = saveOffer(entityManager, waldmann, timCook, jubilee);
        Offer sterlingOffer = saveOffer(entityManager, kaweco, dianeGreene, sterling);

        entityManager.getTransaction().commit();
    }

    private Company saveCompany(EntityManager entityManager, String name) {
        Company company = Company.builder()
                .name(name)
                .build();
        entityManager.persist(company);
        return company;
    }

    private User saveUser(EntityManager entityManager,
                          String username,
                          String firstname,
                          String lastname,
                          LocalDate localDate,
                          Role role) {
        User user = User.builder()
                .username(username)
                .personalInfo(PersonalInfo.builder()
                        .firstname(firstname)
                        .lastname(lastname)
                        .birthDate(localDate)
                        .build())
                .role(role)
                .build();
        entityManager.persist(user);
        return user;
    }

    private Pen savePen(EntityManager entityManager,
                        String name,
                        int price,
                        int quantity,
                        String description,
                        Company company) {
        Pen pen = Pen.builder()
                .name(name)
                .price(price)
                .quantity(quantity)
                .description(description)
                .company(company)
                .build();
        entityManager.persist(pen);
        return pen;
    }

    private Offer saveOffer(EntityManager entityManager, Company company, User user, Pen pen){
        Offer offer = Offer.builder()
                .company(company)
                .user(user)
                .pen(pen)
                .created_at(Instant.now())
                .build();
        entityManager.persist(offer);
        return offer;
    }
}
