package org.dmdev.penseller.database.repository;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 * Base abstract class for repository integration tests
 * @author dmytromatvieiev
 * @version 0.0.1
 */
@SpringBootTest
@EnableAutoConfiguration
@ActiveProfiles("test")
public abstract class AbstractBaseIntegrationRepositoryTest {

}
