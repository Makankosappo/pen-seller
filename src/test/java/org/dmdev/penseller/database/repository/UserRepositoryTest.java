package org.dmdev.penseller.database.repository;

import jakarta.persistence.EntityManagerFactory;
import org.dmdev.penseller.database.TestDataImporter;
import org.dmdev.penseller.database.entity.User;
import org.dmdev.penseller.database.repositories.Impl.UserRepositoryImpl;
import org.dmdev.penseller.exceptions.DataNotFoundException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


@TestInstance(PER_CLASS)
public class UserRepositoryTest extends AbstractBaseIntegrationRepositoryTest {

    @Autowired
    private UserRepositoryImpl userRepository;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @BeforeAll
    public void initDb() {
        new TestDataImporter().importData(entityManagerFactory);
    }

    @Test
    void contextLoads() {

    }

    @Test
    void findAllUsers_successExpected() {
        Optional<List<User>> usersOpt = userRepository.getAll();

        List<User> result = usersOpt.orElseThrow(
                () -> new DataNotFoundException("Could not get all users from database")
        );
        assertThat(result).hasSize(5);

        List<String> usernamesResult = result.stream().map(User::fullName).toList();
        assertThat(usernamesResult).containsExactlyInAnyOrder("Bill Gates", "Steve Jobs", "Tim Cook", "Sergey Brin", "Diane Greene");
    }

    @Test
    void findUserById_successExpected() {
        Optional<User> userOpt = userRepository.get(1);

        User user = userOpt.orElseThrow(
                () -> new DataNotFoundException("User with id: 1 does not exist")
        );
        assertThat(user.fullName()).isEqualTo("Bill Gates");
    }

    @Test
    void findUserById_dataNotFoundExceptionExpected() {
        Optional<User> userOpt = userRepository.get(7);

        assertThrows(DataNotFoundException.class,
                () -> userOpt.orElseThrow(
                        () -> new DataNotFoundException("User with id: 1 does not exist")
                ));

    }
}
