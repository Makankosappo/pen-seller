package org.dmdev.penseller;

import org.dmdev.penseller.database.repositories.Impl.UserRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;


@SpringBootTest
@ActiveProfiles("test")
class PenSellerApplicationTests {

	@Autowired
	private UserRepositoryImpl userRepository;

	@Test
	void contextLoads() {
	}

}
