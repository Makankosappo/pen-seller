create table offers
(
    id         bigint generated by default as identity,
    created_at timestamp,
    company_id bigint not null,
    pen_id     bigint not null,
    user_id    bigint not null
);

alter table if exists offers add constraint FK_offers_companies foreign key (company_id) references companies;
alter table if exists offers add constraint FK_offers_pens foreign key (pen_id) references pens;
alter table if exists offers add constraint FK_offers_users foreign key (user_id) references users;