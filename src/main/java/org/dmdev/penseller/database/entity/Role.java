package org.dmdev.penseller.database.entity;

public enum Role {
    USER, ADMIN, SELLER
}
