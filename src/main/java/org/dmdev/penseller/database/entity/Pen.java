package org.dmdev.penseller.database.entity;

import jakarta.persistence.*;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "pens")
public class Pen extends AbstractBaseEntity{

    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;
    private int price;
    private int quantity;
    @Column(length = 4095)
    private String description;
}
