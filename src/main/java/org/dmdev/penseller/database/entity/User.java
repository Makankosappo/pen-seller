package org.dmdev.penseller.database.entity;

import jakarta.persistence.*;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.JOINED)
public class User extends AbstractBaseEntity{

    @Column(nullable = false,
    unique = true)
    private String username;
    @Embedded
    @AttributeOverride(name = "birthDate", column = @Column(name = "birth_date"))
    private PersonalInfo personalInfo;
    @Enumerated(EnumType.STRING)
    private Role role;

    public String fullName(){
        return getPersonalInfo().getFirstname() + " " + getPersonalInfo().getLastname();
    }
}
