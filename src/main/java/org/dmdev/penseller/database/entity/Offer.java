package org.dmdev.penseller.database.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "offers")
public class Offer extends AbstractBaseEntity{

    @OneToOne
    @JoinColumn(name = "pen_id")
    private Pen pen;
    @OneToOne
    @JoinColumn(name = "company_id")
    private Company company;
    private Instant created_at;
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
}
