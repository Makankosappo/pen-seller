package org.dmdev.penseller.database.repositories.Impl;

import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.dmdev.penseller.database.entity.User;
import org.dmdev.penseller.database.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static org.dmdev.penseller.database.entity.QUser.user;

/**
 * User repository implementation
 *
 * @version 0.0.1
 */
@Repository

public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Optional<List<User>> getAll() {
        return Optional.ofNullable(new JPAQuery<User>(entityManager)
                .select(user)
                .from(user)
                .fetch());
    }

    @Override
    public Optional<User> get(long id) {
        return Optional.ofNullable(new JPAQuery<User>(entityManager)
                .select(user)
                .from(user)
                .where(user.id.eq(id))
                .fetchOne());
    }

    @Override
    public void save(User user) {
        entityManager.persist(user);
    }

    @Override
    public void delete(long id) {
        new JPADeleteClause(entityManager, user).where(user.id.eq(id)).execute();

    }
}
