package org.dmdev.penseller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PenSellerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PenSellerApplication.class, args);
	}

}
