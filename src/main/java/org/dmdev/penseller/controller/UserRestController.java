package org.dmdev.penseller.controller;

import jakarta.persistence.EntityManager;
import org.dmdev.penseller.database.entity.User;
import org.dmdev.penseller.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserRestController {

    @Autowired
    private UserServiceImpl userService;

    @GetMapping
    public List<User> allUsersList(){
        List<User> users = userService.getAll().orElse(Collections.emptyList());
        return users;
    }


}
