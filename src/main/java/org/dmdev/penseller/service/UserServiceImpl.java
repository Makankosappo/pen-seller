package org.dmdev.penseller.service;

import org.dmdev.penseller.database.entity.User;
import org.dmdev.penseller.database.repositories.Impl.UserRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * User service implementation
 *
 * @version 0.0.1
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepositoryImpl userRepository;


    @Override
    public Optional<List<User>> getAll() {
        return userRepository.getAll();
    }

    @Override
    public Optional<User> get(long id) {
        return userRepository.get(id);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public void delete(long id) {
        userRepository.delete(id);
    }
}
