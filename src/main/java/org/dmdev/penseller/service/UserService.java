package org.dmdev.penseller.service;

import org.dmdev.penseller.database.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    public Optional<List<User>> getAll();

    public Optional<User> get(long id);

    public void save(User user);

    public void delete(long id);
}
