package org.dmdev.penseller.exceptions;

public class DataExistException extends RuntimeException{
    public DataExistException(String message){
        super(message);
    }
}
